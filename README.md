# Scriptable WG Installer

A clone of https://github.com/angristan/wireguard-install
Likely won't be maintained further.

Notable differences: adding a user is available via command line args:  
```
chmod +x manage.sh
./manage.sh new bob
# (Prints Bobs QR code, after generating his profile)
```
I may add the ability to remove users via the command line too, not sure.
